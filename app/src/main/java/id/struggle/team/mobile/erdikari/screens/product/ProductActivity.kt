package id.struggle.team.mobile.erdikari.screens.product

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import id.struggle.team.mobile.erdikari.BR
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.adapters.RecyclerViewAdapter
import id.struggle.team.mobile.erdikari.base.BaseActivity
import id.struggle.team.mobile.erdikari.databinding.ActivityProductBinding
import id.struggle.team.mobile.erdikari.databinding.ItemProductBinding
import id.struggle.team.mobile.erdikari.extensions.gone
import id.struggle.team.mobile.erdikari.extensions.toCurrency
import id.struggle.team.mobile.erdikari.extensions.visible
import id.struggle.team.mobile.erdikari.model.Product
import id.struggle.team.mobile.erdikari.viewmodel.ProductViewModel
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import timber.log.Timber


@AndroidEntryPoint
class ProductActivity : BaseActivity<ActivityProductBinding>() {
    private val viewModel by viewModels<ProductViewModel>()
    override fun getLayoutId(): Int = R.layout.activity_product
    private lateinit var recyclerAdapter: RecyclerViewAdapter<Product, ItemProductBinding>

    override fun ActivityProductBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
    }

    private fun ActivityProductBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.title = getString(R.string.produk)
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchProducts()
        }
    }

    private fun ActivityProductBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    progressBar.visible()
                    swipeContainer.isRefreshing = true
                    recyclerView.gone()
                } else {
                    progressBar.gone()
                    skeletonLayout.showOriginal()
                    recyclerView.visible()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            productList.observe {
                recyclerAdapter.updateList(this)
            }
        }
    }

    private fun ActivityProductBinding.setupAdapter() {
        recyclerAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_product,
            BR.product
        ) { itemView, itemModel ->
            itemView.itemCatalogueTvTitle.text = itemModel.name
            itemView.itemCatalogueTvPrice.text = itemModel.price.toString().toCurrency()
            Glide.with(applicationContext)
                .load(itemModel.image)
                .into(itemView.itemCatalogueIvImage)

            itemView.root.setOnClickListener {
                startActivity(
                    Intent(applicationContext, ProductDetailActivity::class.java).apply {
                        putExtra(ProductDetailActivity.KEY_ID, itemModel.id.toString())
                    }
                )
            }
        }

        recyclerView.apply {
            layoutManager = GridLayoutManager(applicationContext, 2)
            adapter = recyclerAdapter
        }
    }

}