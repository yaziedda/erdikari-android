package id.struggle.team.mobile.erdikari.repository

import id.struggle.team.mobile.erdikari.model.*
import id.struggle.team.mobile.erdikari.model.blog.Blog
import id.struggle.team.mobile.erdikari.model.home.HomeResponse
import id.struggle.team.mobile.erdikari.model.donation.DonationResponse
import id.struggle.team.mobile.erdikari.model.blog.NewsDetailResponse
import id.struggle.team.mobile.erdikari.model.blog.NewsResponse
import id.struggle.team.mobile.erdikari.model.donation.Donation
import okhttp3.RequestBody
import okhttp3.ResponseBody

interface RemoteRepositoryDao {

    suspend fun getHome(): BaseResponse<Home>
    suspend fun getDonation(): BaseResponse<List<Donation>>
    suspend fun getDonationById(id: String): BaseResponse<Donation>
    suspend fun sendDonasi(map: HashMap<String, String?>?): BaseResponse<String>
    suspend fun getProducts(): BaseResponse<List<Product>>
    suspend fun getProductById(id: String): BaseResponse<Product>
    suspend fun addToCart(map: HashMap<String, String?>?): BaseResponse<String>
    suspend fun getCart(): BaseResponse<List<Cart>>
    suspend fun cartUnchecked(id: String, status: String): BaseResponse<String>
    suspend fun cartUpdateQty(id: String, qty: String): BaseResponse<String>
    suspend fun province(): BaseResponse<List<Province>>
    suspend fun city(id: String): BaseResponse<List<City>>
    suspend fun cost(map: HashMap<String, String?>?): BaseResponse<RajaOngkir>
    suspend fun checkout(map: HashMap<String, String?>?): BaseResponse<String>
    suspend fun history(): BaseResponse<List<TransactionHistory>>
    suspend fun blog(): BaseResponse<List<Blog>>
    suspend fun jasa(): BaseResponse<List<Jasa>>
    suspend fun jasaById(id: String): BaseResponse<Jasa>
    suspend fun tokenUpdate(map: HashMap<String, String?>?): BaseResponse<String>


}