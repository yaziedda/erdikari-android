package id.struggle.team.mobile.erdikari.permission.model

class Permission(var name: String, var granted: Boolean = false, var shouldShowRequestPermissionRationale: Boolean = false)