package id.struggle.team.mobile.erdikari.exception

class LoginException :
    Exception("Terjadi kesalahan saat login. Mungkin sedang terjadi kesalahan pada sistem kami.")