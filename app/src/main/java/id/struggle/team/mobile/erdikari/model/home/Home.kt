package id.struggle.team.mobile.erdikari.model.home


import com.google.gson.annotations.SerializedName

data class Home(
    @SerializedName("banner")
    val banner: List<Banner>,
    @SerializedName("menu")
    val catalogues: List<Catalogue>
)