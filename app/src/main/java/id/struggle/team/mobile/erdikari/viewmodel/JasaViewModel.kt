package id.struggle.team.mobile.erdikari.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.struggle.team.mobile.erdikari.base.BaseViewModel
import id.struggle.team.mobile.erdikari.exception.GeneralException
import id.struggle.team.mobile.erdikari.model.Jasa
import id.struggle.team.mobile.erdikari.model.blog.Blog
import id.struggle.team.mobile.erdikari.repository.AnalyticsRepository
import id.struggle.team.mobile.erdikari.repository.AuthenticationRepository
import id.struggle.team.mobile.erdikari.repository.RemoteRepository
import kotlinx.coroutines.launch

class JasaViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _list = MutableLiveData<MutableList<Jasa>>()
    val list: LiveData<MutableList<Jasa>>
        get() = _list

    fun resetState() {
        setError(null)
    }

    fun fetch() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.jasa()
            _loading.value = false
            if (response.state) {
                val list: MutableList<Jasa> = arrayListOf()
                list.addAll(response.data ?: arrayListOf())
                _list.value = list
            } else {
                _list.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun fetchShare(id: String) = viewModelScope.launch {
        try {
        } catch (exception: Exception) {

        }
    }
}