package id.struggle.team.mobile.erdikari.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.struggle.team.mobile.erdikari.base.BaseViewModel
import id.struggle.team.mobile.erdikari.exception.GeneralException
import id.struggle.team.mobile.erdikari.model.donation.Donation
import id.struggle.team.mobile.erdikari.repository.AnalyticsRepository
import id.struggle.team.mobile.erdikari.repository.AuthenticationRepository
import id.struggle.team.mobile.erdikari.repository.RemoteRepository
import kotlinx.coroutines.launch

class AccountViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _infoPentingList = MutableLiveData<MutableList<Donation>>()
    val donationList: LiveData<MutableList<Donation>>
        get() = _infoPentingList

    fun resetState(){
        setError(null)
    }

    private val _displayName = MutableLiveData<String>()
    val displayName: LiveData<String>
        get() = _displayName

    private val _signOutSuccess: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    val signOutSuccess: LiveData<Boolean>
        get() = _signOutSuccess

    fun isAnonymous(): Boolean {
        return authenticationRepository.isAnonymousUser()
    }

    fun getDisplayName() {
        _displayName.value = when {
            authenticationRepository.currentUser()?.displayName != null ->
                authenticationRepository.currentUser()?.displayName ?: ""
            authenticationRepository.currentUser()?.email != null ->
                authenticationRepository.currentUser()?.email ?: ""
            else -> authenticationRepository.currentUser()!!.uid.substring(0, 5)
        }
    }

    fun signOut() {
        authenticationRepository.signOut()
        _signOutSuccess.value = true
    }
}