package id.struggle.team.mobile.erdikari.service

import android.app.PendingIntent
import android.content.Intent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import id.struggle.team.mobile.erdikari.BuildConfig
import id.struggle.team.mobile.erdikari.extensions.sendNotification
import id.struggle.team.mobile.erdikari.screens.SplashScreenActivity

class PushMessagingService : FirebaseMessagingService() {

    private val channelId: String = "notification_"+ BuildConfig.APPLICATION_ID

    override fun onNewToken(token: String) {
        getSharedPreferences("", MODE_PRIVATE).edit().putString("fcm_token", token).apply()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        val intent = Intent(applicationContext, SplashScreenActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        applicationContext.sendNotification(
            channelId,
            pendingIntent,
            remoteMessage.data["title"].toString(),
            remoteMessage.data["body"].toString()
        )
    }
}