package id.struggle.team.mobile.erdikari.exception

class GeneralException(message: String = "Error") : Exception(message)