package id.struggle.team.mobile.erdikari.model.blog

import com.google.gson.annotations.SerializedName

data class NewsDetailResponse(
    @SerializedName("data")
    val blogData: Blog,
    @SerializedName("state")
    val state: Boolean,
    @SerializedName("message")
    val message: String
)