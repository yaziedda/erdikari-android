package id.struggle.team.mobile.erdikari.repository

import id.struggle.team.mobile.erdikari.model.*
import id.struggle.team.mobile.erdikari.model.blog.Blog
import id.struggle.team.mobile.erdikari.model.home.HomeResponse
import id.struggle.team.mobile.erdikari.model.donation.DonationResponse
import id.struggle.team.mobile.erdikari.model.blog.NewsDetailResponse
import id.struggle.team.mobile.erdikari.model.blog.NewsResponse
import id.struggle.team.mobile.erdikari.model.donation.Donation
import okhttp3.RequestBody
import okhttp3.ResponseBody
import javax.inject.Inject

class RemoteRepository @Inject constructor(
    private val remoteRepositoryDao: RemoteRepositoryDao
) : RemoteRepositoryDao {
    override suspend fun getHome(): BaseResponse<Home> {
        return remoteRepositoryDao.getHome()
    }

    override suspend fun getDonation(): BaseResponse<List<Donation>> {
        return remoteRepositoryDao.getDonation()
    }

    override suspend fun getDonationById(id: String): BaseResponse<Donation> {
        return remoteRepositoryDao.getDonationById(id)
    }

    override suspend fun sendDonasi(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.sendDonasi(map)
    }

    override suspend fun getProducts(): BaseResponse<List<Product>> {
        return remoteRepositoryDao.getProducts()
    }

    override suspend fun getProductById(id: String): BaseResponse<Product> {
        return remoteRepositoryDao.getProductById(id)
    }

    override suspend fun addToCart(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.addToCart(map)
    }

    override suspend fun getCart(): BaseResponse<List<Cart>> {
        return remoteRepositoryDao.getCart()
    }

    override suspend fun cartUnchecked(id: String, status: String): BaseResponse<String> {
        return remoteRepositoryDao.cartUnchecked(id, status)
    }

    override suspend fun cartUpdateQty(id: String, qty: String): BaseResponse<String> {
        return remoteRepositoryDao.cartUpdateQty(id, qty)
    }

    override suspend fun province(): BaseResponse<List<Province>> {
        return remoteRepositoryDao.province()
    }

    override suspend fun city(id: String): BaseResponse<List<City>> {
        return remoteRepositoryDao.city(id)
    }

    override suspend fun cost(map: HashMap<String, String?>?): BaseResponse<RajaOngkir> {
        return remoteRepositoryDao.cost(map)
    }

    override suspend fun checkout(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.checkout(map)
    }

    override suspend fun history(): BaseResponse<List<TransactionHistory>> {
        return remoteRepositoryDao.history()
    }

    override suspend fun blog(): BaseResponse<List<Blog>> {
        return remoteRepositoryDao.blog()
    }

    override suspend fun jasa(): BaseResponse<List<Jasa>> {
        return remoteRepositoryDao.jasa()
    }

    override suspend fun jasaById(id: String): BaseResponse<Jasa> {
        return remoteRepositoryDao.jasaById(id)
    }

    override suspend fun tokenUpdate(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.tokenUpdate(map)
    }

}