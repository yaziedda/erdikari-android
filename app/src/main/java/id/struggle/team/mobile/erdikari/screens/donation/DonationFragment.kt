package id.struggle.team.mobile.erdikari.screens.donation

import android.content.Intent
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.struggle.team.mobile.erdikari.BR
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.adapters.RecyclerViewAdapter
import id.struggle.team.mobile.erdikari.base.BaseFragment
import id.struggle.team.mobile.erdikari.databinding.FragmentDonationBinding
import id.struggle.team.mobile.erdikari.databinding.ItemDonationBinding
import id.struggle.team.mobile.erdikari.extensions.toCurrency
import id.struggle.team.mobile.erdikari.model.donation.Donation
import id.struggle.team.mobile.erdikari.viewmodel.DonationViewModel
import timber.log.Timber

class DonationFragment : BaseFragment<FragmentDonationBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_donation
    private lateinit var recyclerDonationAdapter: RecyclerViewAdapter<Donation, ItemDonationBinding>
    private val viewModel by activityViewModels<DonationViewModel>()

    override fun FragmentDonationBinding.initializeView() {
        setupDonationAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
    }

    private fun FragmentDonationBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    swipeContainer.isRefreshing = true
                    skeletonLayout.showSkeleton()
                } else {
                    skeletonLayout.showOriginal()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            donationList.observe {
                recyclerDonationAdapter.updateList(this)
            }
        }
    }

    private fun FragmentDonationBinding.setupDonationAdapter() {
        recyclerDonationAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_donation,
            BR.donation
        ) { itemView, itemModel ->
            itemView.itemHomeDonationContAmount.text =
                itemModel.prosesDonasi.toString().toCurrency()
            itemView.itemHomeDonationContAmountTotal.text =
                getString(R.string.donasi_terkumpul).format(
                    itemModel.targetDonasi.toString().toCurrency()
                )
            itemView.itemHomeDonationTvTitle.text = itemModel.judul

            val percent = calculatePercentage(
                itemModel.prosesDonasi ?: 0, itemModel.targetDonasi ?: 0
            ).toInt()

            itemView.itemHomeDonationProgres.progress = percent

            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemHomeDonationIvImage)

            itemView.root.setOnClickListener {
                startActivity(
                    Intent(requireContext(), DonationDetailActivity::class.java).apply {
                        putExtra(DonationDetailActivity.KEY_ID, itemModel.id.toString())
                    }
                )
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerDonationAdapter
        }
    }

    private fun calculatePercentage(obtained: Long, total: Long): Long {
        return obtained * 100 / total
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchDonation()
        }
    }

}