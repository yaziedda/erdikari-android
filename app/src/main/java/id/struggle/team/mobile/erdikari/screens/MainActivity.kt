package id.struggle.team.mobile.erdikari.screens

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.base.BaseActivity
import id.struggle.team.mobile.erdikari.databinding.ActivityMainBinding
import id.struggle.team.mobile.erdikari.extensions.setupWithNavController
import id.struggle.team.mobile.erdikari.utils.Constants
import id.struggle.team.mobile.erdikari.utils.StatusBar
import id.struggle.team.mobile.erdikari.viewmodel.MainViewModel

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    companion object {
        const val FRAGMENT_DESTINATION = "FRAGMENT_DESTINATION"
    }

    override fun getLayoutId(): Int = R.layout.activity_main
    private val viewModel by viewModels<MainViewModel>()
    private var currentNavController: LiveData<NavController>? = null
    private val labelUpdateTitle by lazy { getString(R.string.label_update_title) }
    private val labelUpdateDescription by lazy { getString(R.string.label_update_description) }
    private var fragmentDestination: Int = 0

    override fun ActivityMainBinding.initializeView(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) setupBottomNavigationBar()
        StatusBar.setLightStatusBar(window)
        with(viewModel) {
            isNeedUpdate.observe {
                if (this) {
                    showDialog(
                        title = labelUpdateTitle,
                        message = labelUpdateDescription
                    ) {
                        val browserIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_PLAY_STORE))
                        startActivity(browserIntent)
                    }
                }
            }
            // TODO FORCE UPDATE
//            forceUpdate()
        }
        if (!viewModel.isAnonymous()) {
            FirebaseMessaging.getInstance().subscribeToTopic(viewModel.getEmail())
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        binding.apply {
            setupBottomNavigationBar()
        }
    }

    private fun ActivityMainBinding.setupBottomNavigationBar() {
        val navGraphIds = listOf(
            R.navigation.home_nav,
            R.navigation.donation_nav,
            R.navigation.jasa_nav,
            R.navigation.blog_nav,
            R.navigation.setting_nav
        )
        val controller = mainBottomNav.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.main_nav_host_container,
            intent = intent
        )
        currentNavController = controller
        fragmentDestination = intent.getIntExtra(FRAGMENT_DESTINATION, 0)
        if (fragmentDestination != 0) mainBottomNav.selectedItemId = fragmentDestination
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }

    private fun appInstalledOrNot(context: Context, uri: String): Boolean {
        val pm = context.packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }
}