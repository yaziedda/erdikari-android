package id.struggle.team.mobile.erdikari.screens.auth

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableStringBuilder
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.base.BaseActivity
import id.struggle.team.mobile.erdikari.databinding.ActivityForgotPasswordVeriticationBinding
import id.struggle.team.mobile.erdikari.extensions.click
import id.struggle.team.mobile.erdikari.extensions.color
import id.struggle.team.mobile.erdikari.extensions.underline
import id.struggle.team.mobile.erdikari.utils.StatusBar
import id.struggle.team.mobile.erdikari.viewmodel.ForgotPasswordViewModel
import kotlinx.android.synthetic.main.activity_forgot_password_veritication.*

@AndroidEntryPoint
class ForgotPasswordVerificationActivity :
    BaseActivity<ActivityForgotPasswordVeriticationBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_forgot_password_veritication
    private val viewModel by viewModels<ForgotPasswordViewModel>()
    private lateinit var countDownTimer: CountDownTimer
    private val errorMessage: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(getString(R.string.label_prefix_blocking_description))
            .append(
                getString(R.string.label_here).color(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                    .underline()
                    .click { }
            )
    }

    override fun ActivityForgotPasswordVeriticationBinding.initializeView(savedInstanceState: Bundle?) {
        StatusBar.setLightStatusBar(window)
        fetcher()
        setupObserver()
        setupUI()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            sendVerification(applicationContext)
        }
    }

    private fun ActivityForgotPasswordVeriticationBinding.setupUI() {
        otpView.setOtpCompletionListener {
            btnLogin.isEnabled = true
            viewModel.validateOtp(applicationContext, it)
        }
    }

    private fun ActivityForgotPasswordVeriticationBinding.startCountDown() {
        countDownTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val secs = millisUntilFinished / 1000
                tvLabelRetry.text =
                    "Anda dapat mengirim email kembali dalam waktu\n$secs detik lagi"
                tvRetry.visibility = View.GONE
            }

            override fun onFinish() {
                tvLabelRetry.text = "Belum mendapatkan kode verifikasi? "
                tvRetry.visibility = View.VISIBLE
                tvRetry.text = "Kirim ulang"
                tvRetry.setOnClickListener {
                    viewModel.sendVerification(this@ForgotPasswordVerificationActivity.applicationContext)
                    countDownTimer.start()
                }
            }
        }.start()
    }

    private fun ActivityForgotPasswordVeriticationBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    btnLogin.visibility = View.GONE
                    containerLoading.visibility = View.VISIBLE
                } else {
                    btnLogin.visibility = View.VISIBLE
                    containerLoading.visibility = View.GONE
                }
            }
            startCountDown.observe {
                if (this) {
                    startCountDown()
                }
            }
            error.observe {
                if (this != null) {
                    this@ForgotPasswordVerificationActivity.showDialog(
                        message ?: errorMessage,
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.ic_not_logged_in
                        )
                    ) {
                        btnLogin.isEnabled = false
                        otpView.setText("")
                    }
                }
            }
            otpIsValid.observe {
                if (this) {
                    startActivity(
                        Intent(
                            applicationContext,
                            ForgotUpdatePasswordActivity::class.java
                        )
                    )
                }
            }
        }
    }

    override fun onBackPressed() {

    }

    override fun onDestroy() {
        super.onDestroy()
        if (::countDownTimer.isInitialized)
            countDownTimer.cancel()
    }

}