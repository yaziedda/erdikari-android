package id.struggle.team.mobile.erdikari.repository

import id.struggle.team.mobile.erdikari.model.*
import id.struggle.team.mobile.erdikari.model.blog.Blog
import id.struggle.team.mobile.erdikari.model.donation.Donation
import javax.inject.Inject

class RemoteRepositoryDaoImpl @Inject constructor(
    private val remoteRepositoryService: RemoteRepositoryService
) : RemoteRepositoryDao {
    override suspend fun getHome(): BaseResponse<Home> {
        return remoteRepositoryService.getHome()
    }

    override suspend fun getDonation(): BaseResponse<List<Donation>> {
        return remoteRepositoryService.getDonasi()
    }

    override suspend fun getDonationById(id: String): BaseResponse<Donation> {
        return remoteRepositoryService.getDonasiById(id)
    }

    override suspend fun sendDonasi(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.sendDonasi(map)
    }

    override suspend fun getProducts(): BaseResponse<List<Product>> {
        return remoteRepositoryService.getProducts()
    }

    override suspend fun getProductById(id: String): BaseResponse<Product> {
        return remoteRepositoryService.getProductById(id)
    }

    override suspend fun addToCart(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.addToCart(map)
    }

    override suspend fun getCart(): BaseResponse<List<Cart>> {
        return remoteRepositoryService.getCart()
    }

    override suspend fun cartUnchecked(id: String, status: String): BaseResponse<String> {
        return remoteRepositoryService.cartUnchecked(id, status)
    }

    override suspend fun cartUpdateQty(id: String, qty: String): BaseResponse<String> {
        return remoteRepositoryService.cartUpdateQty(id, qty)
    }

    override suspend fun province(): BaseResponse<List<Province>> {
        return remoteRepositoryService.province()
    }

    override suspend fun city(id: String): BaseResponse<List<City>> {
        return remoteRepositoryService.city(id)
    }

    override suspend fun cost(map: HashMap<String, String?>?): BaseResponse<RajaOngkir> {
        return remoteRepositoryService.cost(map)
    }

    override suspend fun checkout(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.checkout(map)
    }

    override suspend fun history(): BaseResponse<List<TransactionHistory>> {
        return remoteRepositoryService.history()
    }

    override suspend fun blog(): BaseResponse<List<Blog>> {
        return remoteRepositoryService.blog()
    }

    override suspend fun jasa(): BaseResponse<List<Jasa>> {
        return remoteRepositoryService.jasa()
    }

    override suspend fun jasaById(id: String): BaseResponse<Jasa> {
        return remoteRepositoryService.jasaById(id)
    }

    override suspend fun tokenUpdate(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.tokenUpdate(map)
    }
}