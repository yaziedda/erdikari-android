package id.struggle.team.mobile.erdikari.screens.setting

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import dagger.hilt.android.AndroidEntryPoint
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.base.BaseFragment
import id.struggle.team.mobile.erdikari.databinding.FragmentSettingBinding
import id.struggle.team.mobile.erdikari.extensions.clearNotifications
import id.struggle.team.mobile.erdikari.model.TransactionHistory
import id.struggle.team.mobile.erdikari.screens.SplashScreenActivity
import id.struggle.team.mobile.erdikari.screens.auth.LoginActivity
import id.struggle.team.mobile.erdikari.screens.product.TransactionHistoryActivity
import id.struggle.team.mobile.erdikari.utils.DialogUtils
import id.struggle.team.mobile.erdikari.viewmodel.AccountViewModel

@AndroidEntryPoint
class SettingFragment : BaseFragment<FragmentSettingBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_setting
    private val viewModel by activityViewModels<AccountViewModel>()

    override fun FragmentSettingBinding.initializeView() {
        initObserver()
        checkAnonymous()
        initLogout()
        llAccountEditProfile.setOnClickListener {
            startActivity(Intent(requireContext(), TransactionHistoryActivity::class.java))
        }
    }

    private fun FragmentSettingBinding.initObserver() {
        viewModel.displayName.observe {
            tvName.text = this
        }

        viewModel.signOutSuccess.observe(viewLifecycleOwner, { signOutSuccess ->
            if (signOutSuccess) {
                val intent = Intent(activity, SplashScreenActivity::class.java)
                activity?.startActivity(intent)
                activity?.finish()
            }
        })
    }

    private fun FragmentSettingBinding.initLogout() {
        buttonLogout.setOnClickListener {
            requireContext().clearNotifications()
            DialogUtils.showBasicAlertConfirmationDialog(
                activity = requireActivity(),
                ContextCompat.getDrawable(requireContext(), R.drawable.logo),
                "Apakah anda yakin akan logout?"
            ) {
                viewModel.signOut()
            }
        }
    }

    private fun FragmentSettingBinding.checkAnonymous() {
        val isAnonymous = viewModel.isAnonymous()
        if (!isAnonymous) {
            contentAnonymous.visibility = View.GONE
            contentMain.visibility = View.VISIBLE
            viewModel.getDisplayName()
        } else {
            contentAnonymous.visibility = View.VISIBLE
            contentMain.visibility = View.GONE
            contentAnonymous.apply {
                buttonLogin.setOnClickListener {
                    startActivity(Intent(requireActivity(), LoginActivity::class.java))
                }

            }
        }
    }


}