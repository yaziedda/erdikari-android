package id.struggle.team.mobile.erdikari.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.struggle.team.mobile.erdikari.base.BaseViewModel
import id.struggle.team.mobile.erdikari.exception.GeneralException
import id.struggle.team.mobile.erdikari.model.Banner
import id.struggle.team.mobile.erdikari.model.Home
import id.struggle.team.mobile.erdikari.model.home.Catalogue
import id.struggle.team.mobile.erdikari.model.Product
import id.struggle.team.mobile.erdikari.model.donation.Donation
import id.struggle.team.mobile.erdikari.repository.AnalyticsRepository
import id.struggle.team.mobile.erdikari.repository.AuthenticationRepository
import id.struggle.team.mobile.erdikari.repository.RemoteRepository
import kotlinx.coroutines.launch

class HomeViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _isNeedUpdate = MutableLiveData<Boolean>()
    val isNeedUpdate: LiveData<Boolean>
        get() = _isNeedUpdate

    private val _home = MutableLiveData<Home?>()
    val home: LiveData<Home?>
        get() = _home

    private val _catalogueList = MutableLiveData<MutableList<Catalogue>>()
    val catalogueList: LiveData<MutableList<Catalogue>>
        get() = _catalogueList

    private val _bannerList = MutableLiveData<MutableList<Banner>>()
    val bannerList: LiveData<MutableList<Banner>>
        get() = _bannerList

    private val _productList = MutableLiveData<MutableList<Product>>()
    val productList: LiveData<MutableList<Product>>
        get() = _productList

    private val _donationList = MutableLiveData<MutableList<Donation>>()
    val donationList: LiveData<MutableList<Donation>>
        get() = _donationList


    fun resetState() {
        _bannerList.value = arrayListOf()
        _productList.value = arrayListOf()
        _donationList.value = arrayListOf()
        _catalogueList.value = arrayListOf()
        _home.value = null
        setError(null)
    }

    fun fetchHome() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getHome()
            _loading.value = false
            if (response.state) {

                val listBanner : MutableList<Banner> = arrayListOf()
                listBanner.addAll(response.data?.banner ?: arrayListOf())
                _bannerList.value = listBanner

                val listProduct : MutableList<Product> = arrayListOf()
                listProduct.addAll(response.data?.product ?: arrayListOf())
                _productList.value = listProduct

                val listDonation : MutableList<Donation> = arrayListOf()
                listDonation.addAll(response.data?.donasi ?: arrayListOf())
                _donationList.value = listDonation

            } else {
                _home.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun isAnonymous() = authenticationRepository.isAnonymousUser()

}