package id.struggle.team.mobile.erdikari.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.struggle.team.mobile.erdikari.base.BaseViewModel
import id.struggle.team.mobile.erdikari.exception.GeneralException
import id.struggle.team.mobile.erdikari.model.Product
import id.struggle.team.mobile.erdikari.model.donation.Donation
import id.struggle.team.mobile.erdikari.repository.AnalyticsRepository
import id.struggle.team.mobile.erdikari.repository.AuthenticationRepository
import id.struggle.team.mobile.erdikari.repository.RemoteRepository
import kotlinx.coroutines.launch

class DonationViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _donationList = MutableLiveData<MutableList<Donation>>()
    val donationList: LiveData<MutableList<Donation>>
        get() = _donationList


    private val _data = MutableLiveData<Donation?>()
    val data: LiveData<Donation?>
        get() = _data

    private val _isSubmit = MutableLiveData<Boolean>()
    val isSubmit: LiveData<Boolean>
        get() = _isSubmit

    private val _loadingCart: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loadingCart: MutableLiveData<Boolean>
        get() = _loadingCart

    private val _nominal: MutableLiveData<String> by lazy {
        MutableLiveData("0")
    }
    val nominal: MutableLiveData<String>
        get() = _nominal

    private val _midtransUrl = MutableLiveData<String?>()
    val midtransUrl: LiveData<String?>
        get() = _midtransUrl

    fun resetState() {
        setError(null)
    }

    fun fetchDonation() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getDonation()
            _loading.value = false
            if (response.state) {
                val listDonation : MutableList<Donation> = arrayListOf()
                listDonation.addAll(response.data ?: arrayListOf())
                _donationList.value = listDonation
            } else {
                _donationList.value = null
                showError(GeneralException(response.message?: ""))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun fetchById(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getDonationById(id)
            _loading.value = false
            if (response.state) {
                _data.value = response.data
            } else {
                _data.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun sendDonasi(map: HashMap<String, String?>?) = viewModelScope.launch {
        _loadingCart.value = true
        try {
            val response = remoteRepository.sendDonasi(map)
            _loadingCart.value = false
            if (response.state) {
                _midtransUrl.value = response.data
            } else {
                _midtransUrl.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loadingCart.value = false
            showError(exception)
        }
    }

    fun isAnonymous(): Boolean = authenticationRepository.isAnonymousUser()

    fun setNominal(string: String) {
        _nominal.value = string
    }
}