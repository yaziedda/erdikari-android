package id.struggle.team.mobile.erdikari.screens.donation

import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.base.BaseActivity
import id.struggle.team.mobile.erdikari.databinding.ActivityDonationDetailBinding
import id.struggle.team.mobile.erdikari.extensions.gone
import id.struggle.team.mobile.erdikari.extensions.toCurrency
import id.struggle.team.mobile.erdikari.extensions.visible
import id.struggle.team.mobile.erdikari.model.donation.Donation
import id.struggle.team.mobile.erdikari.screens.auth.BlockingPageAnonymousFragment
import id.struggle.team.mobile.erdikari.utils.FragmentSupportManager
import id.struggle.team.mobile.erdikari.viewmodel.DonationViewModel
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import timber.log.Timber

@AndroidEntryPoint
class DonationDetailActivity : BaseActivity<ActivityDonationDetailBinding>() {
    override fun getLayoutId(): Int = R.layout.activity_donation_detail
    private val viewModel by viewModels<DonationViewModel>()
    private lateinit var id: String

    companion object {
        const val KEY_ID = "KEY_ID"
    }

    override fun ActivityDonationDetailBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        getExtras()
        initObserver()
        fetcher()
    }

    private fun fetcher() {
        viewModel.fetchById(id)
    }

    private fun getExtras() {
        id = intent?.extras?.getString(KEY_ID) ?: ""
    }

    private fun ActivityDonationDetailBinding.initObserver() {
        with(viewModel) {
            resetState()
            loading.observe {
                setLoading(this)
            }

            data.observe {
                if (this != null) {
                    setDetail(this)
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            isSubmit.observe {
                if (this) {

                }
            }
            loadingCart.observe {
                if (this) {
                    showProgressDialog()
                } else {
                    dissmissProgressDialog()
                }
            }
        }
    }

    private fun ActivityDonationDetailBinding.setDetail(donation: Donation) {
        toolbar.toolbar_support.toolbar_text_title.text = donation.judul
        if (donation.images?.isNotEmpty() == true) {
            activityDonationDetailCarousel.setImageListener { position, imageView ->
                val url = donation.images.getOrNull(position) ?: ""
                Glide.with(applicationContext)
                    .load(url)
                    .into(imageView)

                imageView.setOnClickListener {
                    goToShowImageActivity(url)
                }
            }
            activityDonationDetailCarousel.pageCount = donation.images.size
        }

        activityDonationDetailcontAmount.text =
            donation.prosesDonasi.toString().toCurrency()
        activityDonationDetailcontAmountTotal.text =
            getString(R.string.donasi_terkumpul).format(
                donation.targetDonasi.toString().toCurrency()
            )
        activityDonationDetailtvTitle.text = donation.judul

        val percent = calculatePercentage(
            donation.prosesDonasi ?: 0, donation.targetDonasi ?: 0
        ).toInt()

        activityDonationDetailprogres.progress = percent

        buttonDonation.setOnClickListener {
            if (!viewModel.isAnonymous()) {
                startActivity(
                    Intent(applicationContext, DonationNumpadActivity::class.java).apply {
                        putExtra(DonationNumpadActivity.KEY_ID, donation.id.toString())
                    }
                )
            } else {
                FragmentSupportManager(
                    this@DonationDetailActivity,
                    BlockingPageAnonymousFragment()
                ).show()
            }
        }
    }

    private fun calculatePercentage(obtained: Long, total: Long): Long {
        return obtained * 100 / total
    }

    private fun ActivityDonationDetailBinding.setLoading(
        b: Boolean
    ) {
        if (b) {
            progressBar.visible()
            activityDonationDetailWrapper.gone()
        } else {
            progressBar.gone()
            activityDonationDetailWrapper.visible()
        }
    }

    private fun ActivityDonationDetailBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
        }
    }
}