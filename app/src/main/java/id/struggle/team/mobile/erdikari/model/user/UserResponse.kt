package id.struggle.team.mobile.erdikari.model.user

import id.struggle.team.mobile.erdikari.model.user.User

data class UserResponse(
    val `data`: User,
    val message: String,
    val state: Boolean,
    val token: String
)