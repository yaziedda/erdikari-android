package id.struggle.team.mobile.erdikari.screens.product

import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.struggle.team.mobile.erdikari.BR
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.adapters.RecyclerViewAdapter
import id.struggle.team.mobile.erdikari.base.BaseFragment
import id.struggle.team.mobile.erdikari.databinding.FragmentProductBinding
import id.struggle.team.mobile.erdikari.databinding.ItemProductBinding
import id.struggle.team.mobile.erdikari.extensions.toCurrency
import id.struggle.team.mobile.erdikari.model.Product
import id.struggle.team.mobile.erdikari.viewmodel.ProductViewModel
import timber.log.Timber

class ProductFragment: BaseFragment<FragmentProductBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_product
    private lateinit var recyclerAdapter: RecyclerViewAdapter<Product, ItemProductBinding>
    private val viewModel by activityViewModels<ProductViewModel>()
    override fun FragmentProductBinding.initializeView() {
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchProducts()
        }
    }

    private fun FragmentProductBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    swipeContainer.isRefreshing = true
                    skeletonLayout.showSkeleton()
                } else {
                    skeletonLayout.showOriginal()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            productList.observe {
                recyclerAdapter.updateList(this)
            }
        }
    }
    private fun FragmentProductBinding.setupAdapter() {
        recyclerAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_product,
            BR.product
        ) { itemView, itemModel ->
            itemView.itemCatalogueTvTitle.text = itemModel.name
            itemView.itemCatalogueTvPrice.text = itemModel.price.toString().toCurrency()
            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemCatalogueIvImage)
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerAdapter
        }
    }
}