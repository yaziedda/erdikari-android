package id.struggle.team.mobile.erdikari.screens.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.bumptech.glide.Glide
import id.struggle.team.mobile.erdikari.BR
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.adapters.RecyclerViewAdapter
import id.struggle.team.mobile.erdikari.base.BaseFragment
import id.struggle.team.mobile.erdikari.databinding.FragmentHomeBinding
import id.struggle.team.mobile.erdikari.databinding.ItemDonationHorizontalBinding
import id.struggle.team.mobile.erdikari.databinding.ItemHomeBannerBinding
import id.struggle.team.mobile.erdikari.databinding.ItemProductHorizontalBinding
import id.struggle.team.mobile.erdikari.extensions.toCurrency
import id.struggle.team.mobile.erdikari.model.Banner
import id.struggle.team.mobile.erdikari.model.Product
import id.struggle.team.mobile.erdikari.model.donation.Donation
import id.struggle.team.mobile.erdikari.screens.auth.BlockingPageAnonymousFragment
import id.struggle.team.mobile.erdikari.screens.donation.DonationDetailActivity
import id.struggle.team.mobile.erdikari.screens.donation.DonationNumpadActivity
import id.struggle.team.mobile.erdikari.screens.feature.WebViewActivity
import id.struggle.team.mobile.erdikari.screens.product.CartActivity
import id.struggle.team.mobile.erdikari.screens.product.ProductActivity
import id.struggle.team.mobile.erdikari.screens.product.ProductDetailActivity
import id.struggle.team.mobile.erdikari.utils.FragmentSupportManager
import id.struggle.team.mobile.erdikari.utils.StatusBar
import id.struggle.team.mobile.erdikari.viewmodel.HomeViewModel
import timber.log.Timber

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_home
    private val viewModel by activityViewModels<HomeViewModel>()
    private lateinit var recyclerBannerViewAdapter: RecyclerViewAdapter<Banner, ItemHomeBannerBinding>
    private lateinit var recyclerProductViewAdapter: RecyclerViewAdapter<Product, ItemProductHorizontalBinding>
    private lateinit var recyclerDonationViewAdapter: RecyclerViewAdapter<Donation, ItemDonationHorizontalBinding>

    override fun FragmentHomeBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        ivCart.setOnClickListener {
            if (!viewModel.isAnonymous()) {
                startActivity(Intent(requireContext(), CartActivity::class.java))
            } else {
                FragmentSupportManager(
                    requireActivity(),
                    BlockingPageAnonymousFragment()
                ).show()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        StatusBar.setStatusBar(
            requireActivity().window,
            ContextCompat.getColor(requireContext(), R.color.colorPrimary)
        )
        setupUI()
        setupObserver()
        fetcher()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchHome()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    binding.apply {
                        containerLoading.visibility = View.VISIBLE
                        fragmentHomeWrapperContainer.visibility = View.GONE
                        skeletonLayout.showSkeleton()
                    }
                } else {
                    binding.apply {
                        containerLoading.visibility = View.GONE
                        fragmentHomeWrapperContainer.visibility = View.VISIBLE
                        skeletonLayout.showOriginal()
                        swipeContainer.isRefreshing = false
                    }
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        println("asu error mulu " + this.message)
                        fetcher()
                    }
                }
            }


            bannerList.observe {
                recyclerBannerViewAdapter.updateList(this)
            }

            productList.observe {
                recyclerProductViewAdapter.updateList(this)
            }

            donationList.observe {
                recyclerDonationViewAdapter.updateList(this)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI() {
        binding.apply {
            setupBannerAdapter()
            setupProductAdapter()
            setupDonationAdapter()

            productViewAll.setOnClickListener {
                startActivity(Intent(requireContext(), ProductActivity::class.java))
            }

            donasiViewAll.setOnClickListener {

            }
        }
    }

    private fun FragmentHomeBinding.setupBannerAdapter() {
        recyclerBannerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_home_banner,
            BR.banner
        ) { itemView, itemModel ->
            Glide.with(requireActivity())
                .load(itemModel.file)
                .into(itemView.fragmentHomeBannerImage)
            itemView.fragmentHomeBannerImage.setOnClickListener {
                val intent = Intent(requireContext(), WebViewActivity::class.java)
                intent.putExtra("value", "https://google.com")
                intent.putExtra("name", "")
                startActivity(intent)
            }
        }
        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        fragmentHomeBannerRecyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerBannerViewAdapter
        }
        LinearSnapHelper().attachToRecyclerView(fragmentHomeBannerRecyclerView)
        indicator.attachToRecyclerView(fragmentHomeBannerRecyclerView)
    }

    private fun FragmentHomeBinding.setupProductAdapter() {
        recyclerProductViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_product_horizontal,
            BR.product
        ) { itemView, itemModel ->
            itemView.itemCatalogueTvTitle.text = itemModel.name
            itemView.itemCatalogueTvPrice.text = itemModel.price.toString().toCurrency()
            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemCatalogueIvImage)
            itemView.root.setOnClickListener {
                startActivity(
                    Intent(requireContext(), ProductDetailActivity::class.java).apply {
                        putExtra(ProductDetailActivity.KEY_ID, itemModel.id.toString())
                    }
                )
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        fragmentHomeRecyclerViewLayanan.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerProductViewAdapter
        }
    }


    private fun FragmentHomeBinding.setupDonationAdapter() {
        recyclerDonationViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_donation_horizontal,
            BR.donation
        ) { itemView, itemModel ->
            itemView.itemHomeDonationContAmount.text =
                itemModel.prosesDonasi.toString().toCurrency()
            itemView.itemHomeDonationContAmountTotal.text =
                getString(R.string.donasi_terkumpul).format(
                    itemModel.targetDonasi.toString().toCurrency()
                )
            itemView.itemHomeDonationTvTitle.text = itemModel.judul

            val percent = calculatePercentage(
                itemModel.prosesDonasi ?: 0, itemModel.targetDonasi ?: 0
            ).toInt()

            itemView.itemHomeDonationProgres.progress = percent

            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemHomeDonationIvImage)

            itemView.buttonDonation.setOnClickListener {
                startActivity(
                    Intent(requireContext(), DonationDetailActivity::class.java).apply {
                        putExtra(DonationDetailActivity.KEY_ID, itemModel.id.toString())
                    }
                )
            }

            itemView.root.setOnClickListener {
                startActivity(
                    Intent(requireContext(), DonationDetailActivity::class.java).apply {
                        putExtra(DonationDetailActivity.KEY_ID, itemModel.id.toString())
                    }
                )
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        fragmentHomeRecyclerViewNews.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerDonationViewAdapter
        }
    }

    private fun calculatePercentage(obtained: Long, total: Long): Long {
        return obtained * 100 / total
    }

}