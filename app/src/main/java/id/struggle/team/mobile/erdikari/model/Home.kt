package id.struggle.team.mobile.erdikari.model

import com.google.gson.annotations.SerializedName
import id.struggle.team.mobile.erdikari.model.donation.Donation

data class Home(
    @field:SerializedName("banner")
    val banner: List<Banner> = arrayListOf(),
    @field:SerializedName("product")
    val product: List<Product> = arrayListOf(),
    @field:SerializedName("donasi")
    val donasi: List<Donation> = arrayListOf()
)