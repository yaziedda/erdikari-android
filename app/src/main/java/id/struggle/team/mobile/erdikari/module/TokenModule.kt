package id.struggle.team.mobile.erdikari.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import id.struggle.team.mobile.erdikari.model.Token
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class TokenModule {

    @Singleton
    @Provides
    fun provideToken(): Token = Token()
}