package id.struggle.team.mobile.erdikari.base

interface ToolbarView {
    fun showToolbar(title: CharSequence)
}