package id.struggle.team.mobile.erdikari.repository

import id.struggle.team.mobile.erdikari.model.*
import id.struggle.team.mobile.erdikari.model.blog.Blog
import id.struggle.team.mobile.erdikari.model.home.HomeResponse
import id.struggle.team.mobile.erdikari.model.donation.DonationResponse
import id.struggle.team.mobile.erdikari.model.blog.NewsDetailResponse
import id.struggle.team.mobile.erdikari.model.blog.NewsResponse
import id.struggle.team.mobile.erdikari.model.donation.Donation
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

interface RemoteRepositoryService {

    @GET("home")
    suspend fun getHome(): BaseResponse<Home>

    @GET("donasi")
    suspend fun getDonasi(): BaseResponse<List<Donation>>

    @GET("donasi/{id}")
    suspend fun getDonasiById(@Path("id") id: String): BaseResponse<Donation>

    @FormUrlEncoded
    @POST("checkout-donation")
    suspend fun sendDonasi(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>

    @GET("product")
    suspend fun getProducts(): BaseResponse<List<Product>>

    @GET("product/{id}")
    suspend fun getProductById(@Path("id") id: String): BaseResponse<Product>

    @FormUrlEncoded
    @POST("add-to-cart")
    suspend fun addToCart(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>

    @GET("cart")
    suspend fun getCart(): BaseResponse<List<Cart>>

    @GET("cartUnchecked/{id}/{status}")
    suspend fun cartUnchecked(@Path("id") id: String, @Path("status") status: String): BaseResponse<String>

    @GET("cartUpdateQty/{id}/{qty}")
    suspend fun cartUpdateQty(@Path("id") id: String, @Path("qty") qty: String): BaseResponse<String>

    @GET("province")
    suspend fun province(): BaseResponse<List<Province>>


    @GET("city/{id}")
    suspend fun city(@Path("id") id: String): BaseResponse<List<City>>

    @FormUrlEncoded
    @POST("cost")
    suspend fun cost(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<RajaOngkir>

    @FormUrlEncoded
    @POST("checkout")
    suspend fun checkout(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>

    @GET("history")
    suspend fun history(): BaseResponse<List<TransactionHistory>>

    @GET("blog")
    suspend fun blog(): BaseResponse<List<Blog>>

    @GET("jasa")
    suspend fun jasa(): BaseResponse<List<Jasa>>


    @GET("jasa/{id}")
    suspend fun jasaById(@Path("id") id: String): BaseResponse<Jasa>

    @FormUrlEncoded
    @POST("token-updated")
    suspend fun tokenUpdate(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>
}