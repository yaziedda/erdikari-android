package id.struggle.team.mobile.erdikari.model.user

data class UserToken(
    val user: User,
    val token: String
)