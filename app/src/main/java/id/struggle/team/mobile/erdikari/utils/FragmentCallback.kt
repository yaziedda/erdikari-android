package id.struggle.team.mobile.erdikari.utils

interface FragmentCallback<T> {
    fun result(result: T) {}
    fun action() {}
}