package id.struggle.team.mobile.erdikari.utils

import androidx.fragment.app.Fragment
import id.struggle.team.mobile.erdikari.transition.TransitionType

interface FragmentHelper<Result> {
    val fragment: Fragment
    var animation: TransitionType
    var fragmentCallback: FragmentCallback<Result>?
    var isAlreadyAdded: Boolean

    fun closeFragment() {
        fragment.parentFragmentManager.beginTransaction().setCustomAnimations(
            animation.enterTransition,
            animation.exitTransition
        ).remove(fragment).commit()
        isAlreadyAdded = false
    }
}