package id.struggle.team.mobile.erdikari.screens.blog

import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.struggle.team.mobile.erdikari.BR
import id.struggle.team.mobile.erdikari.R
import id.struggle.team.mobile.erdikari.adapters.RecyclerViewAdapter
import id.struggle.team.mobile.erdikari.base.BaseFragment
import id.struggle.team.mobile.erdikari.databinding.FragmentBlogBinding
import id.struggle.team.mobile.erdikari.databinding.ItemBlogBinding
import id.struggle.team.mobile.erdikari.model.blog.Blog
import id.struggle.team.mobile.erdikari.viewmodel.NewsViewModel
import timber.log.Timber

class BlogFragment : BaseFragment<FragmentBlogBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_blog
    private lateinit var recyclerBlogViewAdapter: RecyclerViewAdapter<Blog, ItemBlogBinding>
    private val viewModel by activityViewModels<NewsViewModel>()

    override fun FragmentBlogBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupNewsAdapter()
        setupObserver()
        fetcher()
    }

    private fun FragmentBlogBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    swipeContainer.isRefreshing = true
                    skeletonLayout.showSkeleton()
                } else {
                    skeletonLayout.showOriginal()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        println("asu error mulu " + this.message)
                        fetcher()
                    }
                }
            }

            list.observe {
                recyclerBlogViewAdapter.updateList(this)
            }
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetch()
        }
    }

    private fun FragmentBlogBinding.setupNewsAdapter() {
        recyclerBlogViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_blog,
            BR.blog
        ) { itemView, itemModel ->

            itemView.itemBlogTvTitle.text = itemModel.judulBlog
            itemView.itemBlogTitle2.text = itemModel.createdAt

            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemBlogIvImage)

            itemView.root.setOnClickListener {
                goToWebViewActivity(itemModel.urlBlog, itemModel.judulBlog ?: "")
            }
        }

        val linearLayoutManager = LinearLayoutManager(requireActivity())
        rvBerita.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerBlogViewAdapter
        }
    }


}