package id.struggle.team.mobile.erdikari.model


import com.google.gson.annotations.SerializedName

data class Banner(
    @SerializedName("created_at")
    val createdAt: String? = "",
    @SerializedName("file")
    val `file`: String? = "",
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("status")
    val status: Int? = 0,
    @SerializedName("updated_at")
    val updatedAt: String? = ""
)