package id.struggle.team.mobile.erdikari.base

interface NavigationConfig {
    fun setAppbarTitle(title: String) {}
}