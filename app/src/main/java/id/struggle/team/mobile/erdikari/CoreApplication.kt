package id.struggle.team.mobile.erdikari

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class CoreApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        configureTimber()


    }

    private fun configureTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}